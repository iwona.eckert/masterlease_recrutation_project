<?php

namespace App\Services;

use Closure;
use Illuminate\Support\Facades\Cache;

class RedisCacheProvider
{
    public function getKeyOrSetIfNoneExist(string $key, Closure $callback, int $ttl = 86400)
    {
        $value = Cache::store('redis')->get($key);

        if (is_string($value)) {
            return json_decode($value, true);
        }

        if (is_null($value) && is_callable($callback)) {
            $value = $callback();

            Cache::store('redis')->put($key, json_encode($value), $ttl);
        }

        return $value;
    }
}
