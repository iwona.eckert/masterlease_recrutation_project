<?php

namespace App\Command;

class MultiplicationCommand
{
    public static function multiplication(int $size): array
    {
        $result = [];

        for ($i = 1; $i <= $size; $i++) {
            for ($j = 1; $j <= $size; $j++) {
                $result[$i][$j] = $i * $j;
            }
        }

        return $result;
    }
}
