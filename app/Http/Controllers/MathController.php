<?php

namespace App\Http\Controllers;

use App\Http\Queries\GetMultiplicationDataEvent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MathController extends Controller
{
    public function multiplication(Request $request): JsonResponse
    {
        $size = $request->query->get('size');

        try {
            $request->validate([
                'size' => 'required|integer|min:1|max:100',
            ]);
        } catch (ValidationException $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                ],
                400
            );
        }

        try{
            $query = new GetMultiplicationDataEvent($size);
            event($query);

        } catch (\Throwable $exception) {
            return response()->json(
                [
                    'message' => $exception->getMessage(),
                ],
                400
            );
        }

        return response()->json([
            'result' => $query->getResult(),
        ]);
    }
}
