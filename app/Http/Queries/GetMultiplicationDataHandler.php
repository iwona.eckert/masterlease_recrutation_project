<?php

namespace App\Http\Queries;

use App\Command\MultiplicationCommand;
use App\Services\RedisCacheProvider;

class GetMultiplicationDataHandler
{
    private RedisCacheProvider $redisProvider;

    public function __construct(RedisCacheProvider $redisProvider)
    {
        $this->redisProvider = $redisProvider;
    }

    public function handle(GetMultiplicationDataEvent $event): void
    {
        $size = $event->getSize();

        $result = $this->redisProvider->getKeyOrSetIfNoneExist(
            sprintf('multiplication_result_for_size_%d', $size),
            fn() => MultiplicationCommand::multiplication($size),
            86400
        );
        $event->setResult($result);
    }
}
