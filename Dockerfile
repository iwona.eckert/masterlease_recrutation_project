FROM php:8.2-fpm

# Instalacja potrzebnych pakietów i rozszerzeń PHP
RUN apt-get update && apt-get install -y \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    curl \
    git \
    libzip-dev \
    libpq-dev \
    && docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

# Instalacja Composera
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Ustawienie katalogu roboczego
WORKDIR /var/www/html

# Kopiowanie aplikacji do kontenera
COPY . /var/www/html

# Nadawanie uprawnień
RUN chown -R www-data:www-data /var/www/html \
    && chmod -R 775 /var/www/html
