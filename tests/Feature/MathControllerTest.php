<?php

namespace Tests\Feature;

use Tests\TestCase;

class MathControllerTest extends TestCase
{
    public function test_if_size_valid(): void
    {
        $response = $this->get('/api/multiplication?size=4');

        $response->assertContent('{"result":{"1":{"1":1,"2":2,"3":3,"4":4},"2":{"1":2,"2":4,"3":6,"4":8},"3":{"1":3,"2":6,"3":9,"4":12},"4":{"1":4,"2":8,"3":12,"4":16}}}');
        $response->assertStatus(200);
    }

    public function test_int(): void
    {
        $response = $this->get('/api/multiplication?size=abc');

        $response->assertContent('{"message":"The size field must be an integer."}');
        $response->assertStatus(400);
    }

    public function test_max_size(): void
    {
        $response = $this->get('/api/multiplication?size=102');

        $response->assertContent('{"message":"The size field must not be greater than 100."}');
        $response->assertStatus(400);
    }

    public function test_min_size(): void
    {
        $response = $this->get('/api/multiplication?size=-5');

        $response->assertContent('{"message":"The size field must be at least 1."}');
        $response->assertStatus(400);
    }

    public function test_if_size_is_empty(): void
    {
        $response = $this->get('/api/multiplication?size=');

        $response->assertContent('{"message":"The size field is required."}');
        $response->assertStatus(400);
    }

    public function test_if_hasnot_size(): void
    {
        $response = $this->get('/api/multiplication');

        $response->assertContent('{"message":"The size field is required."}');
        $response->assertStatus(400);
    }
}
