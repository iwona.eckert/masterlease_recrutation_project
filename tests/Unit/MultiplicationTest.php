<?php

namespace Tests\Unit;

use App\Command\MultiplicationCommand;
use PHPUnit\Framework\TestCase;

class MultiplicationTest extends TestCase
{
    public function test_multiplication(): void
    {
        $table = [
            '1' => ['1' => 1, '2' => 2],
            '2' => ['1' => 2, '2' => 4]
            ];

        $result = MultiplicationCommand::multiplication(2);
        $this->assertEquals($result, $table);
    }
}
